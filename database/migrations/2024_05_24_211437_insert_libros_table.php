<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::table('libros')->insert([
            'titulo' => 'Fuente Ovejuna',
            'editorial' => 'Gama',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('libros')->insert([
            'titulo' => 'La palabra del mudo',
            'editorial' => 'Novell',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
