<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Libro;
class LibrosController extends Controller
{
    public function index(Request $request)
    {
        $libros = Libro::all();
        
        return view('libros.index', compact('libros'));
    }

    public function create(Request $request)
    {
        
        return view('libros.create');
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'titulo' => 'required',
            'editorial' => 'required',           
        ]);

        try{
            Libro::create($request->all());
            return redirect()->route('libros.index')->with('msn_success', 'Operacion Satisfactoria !!!');

        }catch(\Exception $e){
            $fechaHoraActual = date("Y-m-d H:i:s");
            $mensaje=$fechaHoraActual.' No se puede crear el registro.';
            return redirect()->route('libros.create')->with('msn_error', $mensaje);
        }
    }


    public function edit($id)
    {
        $page = request()->query('page', 1);
        $libro = Libro::findOrFail($id);
        return view('libros.edit', compact('libro', 'page'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'editorial' => 'required',
        ]);

        $page = request()->query('page', 1);

        try {
            $curso = Libro::findOrFail($id);
            $curso->update([
                'titulo' => $request->titulo,
                'editorial' => $request->editorial,
            ]);

            return redirect()->route('libros.index', ['page' => $page])->with('msn_success', 'Operación Satisfactoria !!!');

        } catch (\Exception $e) {
            //LogHelper::logError($this, $e);

            $fechaHoraActual = date("Y-m-d H:i:s");
            $mensaje = $fechaHoraActual . ' No se puede eliminar el registro !!!';

            return redirect()->route('libros.edit', [$id, 'page' => $page])->with('msn_error', $mensaje);
        }
    }

}
