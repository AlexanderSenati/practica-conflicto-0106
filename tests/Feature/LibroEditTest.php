<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Libro;

class LibroEditTest extends TestCase
{
    use RefreshDatabase;

    public function test_libro_update_success(): void
    {
        // Crear un libro inicial manualmente
        $libro = new Libro();
        $libro->titulo = 'Título inicial';
        $libro->editorial = 'Editorial inicial';
        $libro->save();

        // Datos para actualizar el libro
        $libroData = [
            'titulo' => 'libro1',
            'editorial' => 'libro editorial1',
        ];

        // Asumimos que la ruta para actualizar un libro es 'libros.update'
        $response = $this->put(route('libros.update', [$libro->id, 'page' => 1]), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.index', ['page' => 1]));
        $response->assertSessionHas('msn_success', 'Operación Satisfactoria !!!');

        $this->assertDatabaseHas('libros', [
            'id' => $libro->id,
            'titulo' => 'libro1',
            'editorial' => 'libro editorial1',
        ]);
    }


    public function test_libro_update_exception(): void
    {
        // Crear un libro inicial manualmente
        $libro = new Libro();
        $libro->titulo = 'Título inicial';
        $libro->editorial = 'Editorial inicial';
        $libro->save();

        // Datos incorrectos para generar una excepción (nombre muy largo)
        $titulo = str_repeat('a', 256);
        $libroData = [
            'titulo' => $titulo,
            'editorial' => 'editorial válida',
        ];

        // Asumimos que la ruta para actualizar un libro es 'libros.update'
        $response = $this->put(route('libros.update', [$libro->id, 'page' => 1]), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.edit', [$libro->id, 'page' => 1]));
        $response->assertSessionHas('msn_error');

        $mensajeErrorSession = session('msn_error');
        $mensajeError = "No se puede eliminar el registro !!!";
        $this->assertStringContainsString($mensajeError, $mensajeErrorSession);

        $this->assertDatabaseMissing('libros', [
            'id' => $libro->id,
            'titulo' => $titulo,
        ]);
    }


    public function test_libro_update_validation(): void
    {
        // Crear un libro inicial manualmente
        $libro = new Libro();
        $libro->titulo = 'Título inicial';
        $libro->editorial = 'Editorial inicial';
        $libro->save();

        // Datos incorrectos para generar una validación fallida
        $libroData = [
            'titulo' => '',
            'editorial' => '',
        ];

        // Asumimos que la ruta para actualizar un libro es 'libros.update'
        $response = $this->put(route('libros.update', [$libro->id, 'page' => 1]), $libroData);
        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'titulo',
            'editorial',
        ]);

        $this->assertDatabaseMissing('libros', [
            'id' => $libro->id,
            'titulo' => '',
            'editorial' => '',
        ]);
    }

}
