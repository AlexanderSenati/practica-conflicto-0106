<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LibroCrearTest extends TestCase
{
    use RefreshDatabase;

    public function test_libro_store_success(): void
    {
        $libroData = [
            'titulo' => 'Ciencias',
            'editorial' => 'Texto de prueba',
        ];

        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.index'));
        $response->assertSessionHas('msn_success', 'Operacion Satisfactoria !!!');

        $this->assertDatabaseHas('libros', [
            'titulo' => 'Ciencias',
            'editorial' => 'Texto de prueba',
        ]);
    }

    public function test_libro_store_exception(): void
    {
        $titulo = "Ciencias";
        for ($i = 0; $i < 255; $i++) {
            $titulo .= " Ciencias";
        }

        $libroData = [
            'titulo' => $titulo,
            'editorial' => 'Texto de prueba',
        ];

        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertRedirect(route('libros.create'));
        $response->assertSessionHas('msn_error');

        $mensajeErrorSession = session('msn_error');
        $mensajeError = "No se puede crear el registro";
        $this->assertStringContainsString($mensajeError, $mensajeErrorSession);

        $this->assertDatabaseMissing('libros', $libroData);
    }

    public function test_libro_store_validation(): void
    {
        $libroData = [
            'titulo' => '',
            'editorial' => '',
        ];

        $response = $this->post(route('libros.store'), $libroData);
        $response->assertStatus(302);
        $response->assertSessionHasErrors([
            'titulo',
            'editorial',
        ]);

        $this->assertDatabaseMissing('libros', $libroData);
    }
}
