@extends('layout')

@section('content')
<div class="container">
    <h2>Crear Libro</h2>
    <form method="POST" action="{{ route('libros.store') }}">
        @csrf
        <div class="form-group">
            <label for="titulo">Título</label>
            <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo') }}">
            @error('titulo')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="editorial">Editorial</label>
            <input type="text" class="form-control" id="editorial" name="editorial" value="{{ old('editorial') }}">
            @error('editorial')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('libros.index') }}" class="btn btn-danger">Cancelar</a>
    </form>
</div>

@if(session('msn_error'))
    <script>
        let mensaje = "{{ session('msn_error') }}";
        Swal.fire({
            icon: "error",
            html: `<span style="font-size: 16px;">${mensaje}</span>`,
        });
    </script>
@endif

@endsection

