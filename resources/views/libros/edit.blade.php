@extends('layout')

@section('content')
<div class="container">
    <h2>Editar Libro</h2>
    <form method="POST" action="{{ route('libros.update', [$libro->id, 'page' => $page]) }}">
        @csrf
        @method('PUT') <!-- Utiliza PUT para la actualización -->
        <div class="form-group">
            <label for="titulo">Titulo</label>
            <input type="text" class="form-control" id="titulo" name="titulo" value="{{ old('titulo', $libro->titulo) }}">
            @error('titulo')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="editorial">Editorial</label>
            <textarea class="form-control" id="editorial" name="editorial">{{ old('editorial', $libro->editorial) }}</textarea>
            @error('editorial')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('libros.index', ['page' => $page]) }}" class="btn btn-danger">Cancelar</a>
    </form>
</div>

@if(session('msn_error'))
    <script>
        let mensaje = "{{ session('msn_error') }}";
        Swal.fire({
            icon: "error",
            html: `<span style="font-size: 16px;">${mensaje}</span>`,
        });
    </script>
@endif
@endsection